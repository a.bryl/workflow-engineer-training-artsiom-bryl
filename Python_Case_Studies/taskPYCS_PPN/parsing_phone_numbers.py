import re

# Finding Numbers
phonePattern = re.compile(r'^(\d{3})-(\d{3})-(\d{4})$')
print(phonePattern.search('800-555-1212').groups())  # ('800', '555', '1212')
print(phonePattern.search('800-555-1212-1234'))  # None
# Finding the Extension
phonePattern = re.compile(r'^(\d{3})-(\d{3})-(\d{4})-(\d+)$')
print(phonePattern.search('800-555-1212-1234').groups())  # ('800', '555', '1212', '1234')
print(phonePattern.search('800 555 1212 1234'))  # None
print(phonePattern.search('800-555-1212'))  # None
# Handling Different Separators
phonePattern = re.compile(r'^(\d{3})\D+(\d{3})\D+(\d{4})\D+(\d+)$')
print(phonePattern.search('800 555 1212 1234').groups())  # ('800', '555', '1212', '1234')
phonePattern.search('800-555-1212-1234').groups()  # ('800', '555', '1212', '1234')
print(phonePattern.search('80055512121234'))  # None
print(phonePattern.search('800-555-1212'))  # None
# Handling Numbers Without Separators
phonePattern = re.compile(r'^(\d{3})\D*(\d{3})\D*(\d{4})\D*(\d*)$')
print(phonePattern.search('80055512121234').groups())  # ('800', '555', '1212', '1234')
print(phonePattern.search('800.555.1212 x1234').groups())  # ('800', '555', '1212', '1234')
print(phonePattern.search('800-555-1212').groups())  # ('800', '555', '1212', '')
print(phonePattern.search('(800)5551212 x1234'))  # None
# Handling Leading Characters
phonePattern = re.compile(r'^\D*(\d{3})\D*(\d{3})\D*(\d{4})\D*(\d*)$')
print(phonePattern.search('(800)5551212 ext. 1234').groups())  # ('800', '555', '1212', '1234')
print(phonePattern.search('800-555-1212').groups())  # ('800', '555', '1212', '')
print(phonePattern.search('work 1-(800) 555.1212 #1234'))  # None
# Phone Number, Wherever I May Find Ye
phonePattern = re.compile(r'(\d{3})\D*(\d{3})\D*(\d{4})\D*(\d*)$')
print(phonePattern.search('work 1-(800) 555.1212 #1234').groups())  # ('800', '555', '1212', '1234')
print(phonePattern.search('800-555-1212'))  # ('800', '555', '1212', '')
print(phonePattern.search('80055512121234'))  # ('800', '555', '1212', '1234')
# Parsing Phone Numbers (Final Version)
phonePattern = re.compile(r'''
                # don't match beginning of string, number can start anywhere
    (\d{3})     # area code is 3 digits (e.g. '800')
    \D*         # optional separator is any number of non-digits
    (\d{3})     # trunk is 3 digits (e.g. '555')
    \D*         # optional separator
    (\d{4})     # rest of number is 4 digits (e.g. '1212')
    \D*         # optional separator
    (\d*)       # extension is optional and can be any number of digits
    $           # end of string
    ''', re.VERBOSE)
print(phonePattern.search('work 1-(800) 555.1212 #1234').groups())  # ('800', '555', '1212', '1234'))
print(phonePattern.search('800-555-1212'))  # ('800', '555', '1212', ''))
