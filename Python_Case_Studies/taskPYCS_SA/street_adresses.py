import re


""" Matching at the End of a String """

s = '100 NORTH MAIN ROAD'
print(s.replace('ROAD', 'RD.'))  # 100 NORTH MAIN RD.
s = '100 NORTH BROAD ROAD'
print(s.replace('ROAD', 'RD.'))  # 100 NORTH BRD. RD.
print(s[:-4] + s[-4:].replace('ROAD', 'RD.'))  # 100 NORTH BROAD RD.
print(re.sub('ROAD$', 'RD.', s))  # 100 NORTH BROAD RD.

# Matching Whole Words

s = '100 BROAD'
print(re.sub('ROAD$', 'RD.', s))  # 100 BRD.
print(re.sub('\\bROAD$', 'RD.', s))  # 100 BROAD
print(re.sub(r'\bROAD$', 'RD.', s))  # 100 BROAD
s = '100 BROAD ROAD APT. 3'
print(re.sub(r'\bROAD$', 'RD.', s))  # 100 BROAD ROAD APT. 3
print(re.sub(r'\bROAD\b', 'RD.', s))  # 100 BROAD RD. APT 3
