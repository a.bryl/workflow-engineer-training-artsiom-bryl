# Information about doing exercise Case Studies: Word play

## Questions and answers for requirements

The requirements for exercise Case Studies: Word play are clear. There were no questions while completing the task.

## Information for launching

To launch the code, you don't need to export any modules or libraries.

## Testing data

Testing data is in the attached screenshot.
