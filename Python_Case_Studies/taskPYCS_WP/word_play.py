def words():
    """ function read words from the text file """
    with open('words.txt', 'r') as f:
        for line in f:
            word = line.strip()
            return word


def has_no_e(word):
    """ function check if string contain 'e' """
    for letter in word:
        if letter == 'e':
            return False
    return True


def avoids(word, forbidden):
    """ function check if string not contain forbidden characters """
    for letter in word:
        if letter in forbidden:
            return False
    return True


def uses_only(word, available):
    """ function check if string contain only available characters """
    for letter in word:
        if letter not in available:
            return False
    return True


def is_abecedarian(word):
    """ function check if string contain all characters in alphabetical order """
    previous = word[0]
    for c in word:
        if c < previous:
            return False
        previous = c
    return True


def is_abecedarian(word):
    """ function check if string contain all characters in alphabetical order using recursion """
    if len(word) <= 1:
        return True
    if word[0] > word[1]:
        return False
    return is_abecedarian(word[1:])


def is_abecedarian(word):
    """ function check if string contain all characters in alphabetical order using while loop """
    i = 0
    while i < len(word) - 1:
        if word[i + 1] < word[i]:
            return False
        i = i + 1
    return True


def is_palindrome(word):
    """ function check if string is a palindrome """
    i = 0
    j = len(word) - 1
    while i < j:
        if word[i] != word[j]:
            return False
        i = i + 1
        j = j - 1
    return True
