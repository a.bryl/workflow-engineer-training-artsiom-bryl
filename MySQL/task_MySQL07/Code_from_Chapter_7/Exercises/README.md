## Information about doing task7_SQL

The code with completed tasks is in the 'task7.sql' file.

## Results of the exercises

- The result of the exercise 7_1 is in the screenshot 'Exercise3_1.bmp'.
- The result of the exercise 7_2 is in the screenshot 'Exercise3_2.bmp'.
- The result of the exercise 7_3 is in the screenshot 'Exercise3_3.bmp'.