## Information about doing task3_SQL

The code with completed tasks is in the 'task3.sql' file.

## Results of the exercises

- The result of the exercise 3_1 is in the screenshot 'Exercise3_1.bmp'.
- The result of the exercise 3_2 is in the screenshot 'Exercise3_2.bmp'.
- The result of the exercise 3_3 is in the screenshot 'Exercise3_3.bmp'.
- The result of the exercise 3_4 is in the screenshot 'Exercise3_4.bmp'.