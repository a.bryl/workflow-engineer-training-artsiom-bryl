CREATE TABLE heart_disease (
   age INT,   
   sex INT,   
   chest_pain INT,   
   resting_blood_pressure INT,   
   serum_cholestoral_in_mg_dl INT,   
   fasting_blood_sugar_more_120_mg_dl INT,   
   resting_electrocardiographic_results INT,   
   maximum_heart_rate_achieved INT,   
   exercise_induced_angina INT,   
   oldpeak INT,   
   slope INT,   
   number_of_major_vessels INT,   
   thal INT,   
   target INT
   );

LOAD DATA INFILE '/var/lib/mysql-files/heart.csv'  
INTO TABLE heart_disease  FIELDS TERMINATED BY ','  
IGNORE 1 ROWS;

INSERT INTO heart_disease (age, sex, chest_pain, resting_blood_pressure, serum_cholestoral_in_mg_dl, fasting_blood_sugar_more_120_mg_dl, 
						resting_electrocardiographic_results, maximum_heart_rate_achieved, exercise_induced_angina, oldpeak, slope, 
                        number_of_major_vessels, thal, target)
VALUES(48, 1, 0, 112, 163, 0, 1, 189, 0, 1, 2, 2, 3, 0);

INSERT INTO heart_disease (age, sex, chest_pain, resting_blood_pressure, serum_cholestoral_in_mg_dl, fasting_blood_sugar_more_120_mg_dl, 
						resting_electrocardiographic_results, maximum_heart_rate_achieved, exercise_induced_angina, oldpeak, slope, 
                        number_of_major_vessels, thal, target)
VALUES(85, 0, 0, 135, 170, 0, 1, 153, 0, 1, 2, 2, 3, 0);

INSERT INTO heart_disease (age, sex, chest_pain, resting_blood_pressure, serum_cholestoral_in_mg_dl, fasting_blood_sugar_more_120_mg_dl, 
						resting_electrocardiographic_results, maximum_heart_rate_achieved, exercise_induced_angina, oldpeak, slope, 
                        number_of_major_vessels, thal, target)
VALUES(20, 1, 0, 60, 60, 0, 1, 60, 0, 1, 2, 2, 3, 0);