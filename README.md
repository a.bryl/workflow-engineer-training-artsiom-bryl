# md syntax task

## About me

Hello! My name is Artsiom Bryl. I'm a beginner programmer. I am a former Health and Safety Regulations Leading Engineer of JSC "Belaruskali". I decided to switch to IT and do software development. I study at Polotsk State University with a degree in Information Technology Software.I worked with Python, MySQL, Linux. I like hiking and alpinism.

## My skills

| Skill | Evaluation |
| :---: | :--------: |
| mysql | 2 |
| yaml | 1 |
| csv | 0 |
| md | 3 |
| git | 3 |
| redmine | 0 |
| jira | 1 |
| Python | 3 |

## Freetime

I am free to study 24/7.

## Links

- [LinkedIn](https://www.linkedin.com/in/artsiom-bryl-885026203/)
- [GitHub](https://github.com/artsiombryl)
- [GitLab](https://gitlab.com/a.bryl)
- [Jira](https://workflow-engineers-syberry.atlassian.net/jira/people/604ba61f43eac6006f9fe932)
- Discord - artsiom.bryl#5516
- E-mail - artsiom.bryl@gmail.com
