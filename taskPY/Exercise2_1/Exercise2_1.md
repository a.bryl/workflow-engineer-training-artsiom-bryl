# Exercise 1.1

Repeating my advice from the previous chapter, whenever you learn a new feature, you should try it out in an 
interactive mode and make errors on purpose to see what goes wrong. 
1. We’ve seen that n = 42 is legal. What about 42 = n?
2. How about x = y = 1?
3. In some languages every statement ends with a semi-colon, ;. What happens if you put a semi-colon at the end of 
   a Python statement?
4. What if you put a period at the end of a statement?
5. In math notation you can multiply x and y like this: xy. What happens if you try that in Python?

## Answers

1. At the output we will get a syntax error, because it is an impossible assign a variable to a number.
2. The variable "x" is assigned the value of the variable "y". The variable "y" is assigned the value 1.
3. We will get a syntax error at the output.
4. We will get a syntax error at the output.
5. We will get a syntax error at the output because multiplication in Python is performed only with the "*" sign.
