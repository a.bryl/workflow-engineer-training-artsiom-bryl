""" Exercise 10.4. Write a function called chop that takes a list, modifies it by removing the first and
last elements, and returns None. """


import logging

logger = logging.getLogger()
logger.setLevel(logging.DEBUG)
fileHandler = logging.FileHandler('logs.log')
fileHandler.setFormatter(logging.Formatter(fmt='[%(asctime)s: %(levelname)s] %(message)s'))
logger.addHandler(fileHandler)


def chop(num_list):
    """ function takes a list, modifies it by removing the first and
    last elements, and returns None """
    logging.info("Execute chop() function")
    del x[0]
    del x[-1]


x = [1, 2, 3, 4]
chop(x)
print(x)
