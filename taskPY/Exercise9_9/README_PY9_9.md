# Information about doing exercise 9-9

## Questions and answers for requirements

The requirements for Exercise 9-9 are clear. There were no questions while completing the task.

## Information for launching

To launch the code, you need to export next modules:
- logging

## Testing data

Testing data is in the attached screenshot.
