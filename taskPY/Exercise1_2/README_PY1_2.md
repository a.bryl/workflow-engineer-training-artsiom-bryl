# Information about doing exercise 1-2

## Questions and answers for requirements

The requirements for Exercise 1-2 are clear. There were no questions while completing the task.

## Information for launching

To launch the code, you don't need to install additional components except for Python 3.X.

## Testing data

Testing data is in the attached screenshot.
