""" How many seconds are there in 42 minutes 42 seconds? """

minutes = 42
seconds = 42

total_seconds = minutes * 60 + seconds
print(str(total_seconds) + ' seconds in 42 minutes 42 seconds')

""" How many miles are there in 10 kilometers? Hint: there are 1.61 kilometers in a mile """

kilometers = 10
km_to_miles = 1.61
miles = kilometers / km_to_miles
print(str(miles) + ' miles in 10 kilometers')

"""  If you run a 10 kilometer race in 42 minutes 42 seconds, what is your average pace (time per 
mile in minutes and seconds)? What is your average speed in miles per hour? """

hours = total_seconds / 3600
pace_sec = total_seconds / miles
pace_minutes = round(pace_sec // 60)
pace_seconds = round(pace_sec % 60)
print('Your average pace is ' + str(pace_minutes) + ' minutes and ' + str(pace_seconds) + ' seconds per mile')
