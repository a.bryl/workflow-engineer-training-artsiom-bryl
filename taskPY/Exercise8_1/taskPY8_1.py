""" Exercise 8.1. Read the documentation of the string methods at
http://docs.python.org/3/library/stdtypes.html#string-methods. You might want to experiment with
some of them to make sure you understand how they work. strip and replace are particularly useful.
The documentation uses a syntax that might be confusing. For example, in
find(sub[, start[, end]]), the brackets indicate optional arguments. So sub is required, but
start is optional, and if you include start, then end is optional. """


import logging

logger = logging.getLogger()
logger.setLevel(logging.DEBUG)
fileHandler = logging.FileHandler('logs.log')
fileHandler.setFormatter(logging.Formatter(fmt='[%(asctime)s: %(levelname)s] %(message)s'))
logger.addHandler(fileHandler)


string = "hello, world!"


def main(s):
    logging.info("Execute main function")
    try:
        print(s.capitalize())
        """ Return a copy of the string with its first character capitalized and the rest lowercase """
        print(s.find('l', 0, len(s)))
        """ Return the lowest index in the string where substring sub is found within the slice s[start:end]. 
        Optional arguments start and end are interpreted as in slice notation. Return -1 if sub is not found.) """
        print(s.rfind('l', 0, len(s)))
        """ Return the highest index in the string where substring sub is found, such that sub is contained within 
        s[start:end]. Optional arguments start and end are interpreted as in slice notation. Return -1 on failure. """
        print(s.lower())
        """ Return a copy of the string with all the cased characters converted to lowercase """
        print(s.replace('hello', 'hi'))
        """ Return a copy of the string with all occurrences of substring old replaced by new. If the optional argument 
        count is given, only the first count occurrences are replaced """
        print(s.upper())
        """ Return a copy of the string with all the cased characters converted to uppercase """
        print(s.split())
        """ Return a list of the words in the string """
        print(s.removeprefix('hello, '))
        """ If the string starts with the prefix string, return string[len(prefix):] """
        print(s.removesuffix(', world!'))
        """ If the string ends with the suffix string and that suffix is not empty, return string[:-len(suffix)] """
        print(s.isalpha())
        """ Return True if all characters in the string are alphabetic and there is at least one character, 
        False otherwise """
    except TypeError:
        logging.exception("Argument must be string")


main(string)
