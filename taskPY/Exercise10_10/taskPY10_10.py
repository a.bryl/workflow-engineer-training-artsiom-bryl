""" Exercise 10.10. To check whether a word is in the word list, you could use the in operator, but it
would be slow because it searches through the words in order.
Because the words are in alphabetical order, we can speed things up with a bisection search (also
known as binary search), which is similar to what you do when you look a word up in the dictionary
(the book, not the data structure). You start in the middle and check to see whether the word you are
looking for comes before the word in the middle of the list. If so, you search the first half of the list
the same way. Otherwise you search the second half.
Either way, you cut the remaining search space in half. If the word list has 113,809 words, it will
take about 17 steps to find the word or conclude that it’s not there.
Write a function called in_bisect that takes a sorted list and a target value and returns True if
the word is in the list and False if it’s not. """

import logging
import bisect

logger = logging.getLogger()
logger.setLevel(logging.DEBUG)
fileHandler = logging.FileHandler('logs.log')
fileHandler.setFormatter(logging.Formatter(fmt='[%(asctime)s: %(levelname)s] %(message)s'))
logger.addHandler(fileHandler)


def list1():
    """ function read words from file and build a list using append """
    logging.info("Execute list1() function")
    try:
        my_list = []
        file = open('words.txt')
        for line in file:
            word = line.strip()
            my_list.append(word)
        return my_list
    except FileNotFoundError:
        logging.exception("File word.txt don't exist")


def in_bisect(word, my_list):
    """ function takes a sorted list and a target value and returns True if
    the word is in the list and False if it’s not """
    logging.info("Execute in_bisect() function")
    try:
        i = bisect.bisect_left(my_list, word)
        if i == len(my_list):
            return False
        return my_list[i] == word
    except TypeError:
        logging.exception("Argument must be string")


def main():
    logging.info("Execute main() function")
    my_list = list1()
    print(in_bisect('agony', my_list))
    print(in_bisect(17, my_list))
    print(in_bisect('aaaaaaaaa', my_list))


main()
