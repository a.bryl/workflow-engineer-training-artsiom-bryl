""" Exercise 10.8. If there are 23 students in your class, what are the chances that two of you have
the same birthday? You can estimate this probability by generating random samples of 23 birthdays and
checking for matches. Hint: you can generate random birthdays with the randint function in the random
module. """

import random
import logging

logger = logging.getLogger()
logger.setLevel(logging.DEBUG)
fileHandler = logging.FileHandler('logs.log')
fileHandler.setFormatter(logging.Formatter(fmt='[%(asctime)s: %(levelname)s] %(message)s'))
logger.addHandler(fileHandler)


def random_birthdays(number_of_students):
    """ function takes a number of students (n) and return
    a list of integers between 1 and 365 with length n """
    logging.info("Execute random_birthdays() function")
    try:
        list_of_birthdays = []
        for i in range(number_of_students):
            birthday = random.randint(1, 365)
            list_of_birthdays.append(birthday)
        return list_of_birthdays
    except TypeError:
        logging.exception("Arguments must be numeric")


def has_duplicates(list_of_birthday):
    """ function takes a list of birthday and returns True
    if there is any element that appears more than once """
    logging.info("Execute has_duplicates() function")
    try:
        if len(list_of_birthday) == len(set(list_of_birthday)):
            return False
        else:
            return True
    except TypeError:
        logging.exception("Arguments must be list")


def count_matches(number_of_students, number_of_simulations):
    """ function takes as input the number of students and the number of simulations,
    generates each simulation and counts simulations with duplicate values """
    logging.info("Execute count_matches() function")
    try:
        count = 0
        for i in range(number_of_simulations):
            list_of_birthday = random_birthdays(number_of_students)
            if has_duplicates(list_of_birthday):
                count += 1
        return count
    except TypeError:
        logging.exception("Arguments must be numeric")


def main():
    """ function run the birthday simulation and print the chance of matches """
    logging.info("Execute main() function")
    number_of_students = 23
    number_of_simulations = 1000
    count = count_matches(number_of_students, number_of_simulations)
    chance = count / number_of_simulations * 100
    print(f'After {number_of_simulations} simulations with {number_of_students} students the chance that '
          f'there are at least two people in the class with the same birthday is {chance} %')


main()
