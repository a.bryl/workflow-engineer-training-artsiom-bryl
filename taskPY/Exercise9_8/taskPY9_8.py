""" Exercise 9.8. “I was driving on the highway the other day and I happened to notice my odometer.
Like most odometers, it shows six digits, in whole miles only. So, if my car had 300,000
miles, for example, I’d see 3-0-0-0-0-0.
“Now, what I saw that day was very interesting. I noticed that the last 4 digits were
palindromic; that is, they read the same forward as backward. For example, 5-4-4-5 is a
palindrome, so my odometer could have read 3-1-5-4-4-5.
“One mile later, the last 5 numbers were palindromic. For example, it could have read
3-6-5-4-5-6. One mile after that, the middle 4 out of 6 numbers were palindromic. And
you ready for this? One mile later, all 6 were palindromic!
“The question is, what was on the odometer when I first looked?”
Write a Python program that tests all the six-digit numbers and prints any numbers that satisfy
these requirements. """

import logging

logger = logging.getLogger()
logger.setLevel(logging.DEBUG)
fileHandler = logging.FileHandler('logs.log')
fileHandler.setFormatter(logging.Formatter(fmt='[%(asctime)s: %(levelname)s] %(message)s'))
logger.addHandler(fileHandler)


def palindrome(i, start, length):
    """ function check if the string representation of i is a palindrome """
    logging.info("Execute palindrome() function")
    try:
        s = str(i)[start:start + length]
        return s[::-1] == s
    except TypeError:
        logging.exception("Argument must be numeric")


def check(i):
    """ function check if the i has the desired properties """
    logging.info("Execute check() function")
    try:
        return (palindrome(i, 2, 4) and
                palindrome(i + 1, 1, 5) and
                palindrome(i + 2, 1, 4) and
                palindrome(i + 3, 0, 6))
    except TypeError:
        logging.exception("Argument must be numeric")


def check_all():
    """ enumerate the six-digit numbers and prints numbers matching a condition """
    logging.info("Execute check() function")
    try:
        i = 100000
        while i <= 999999:
            if check(i):
                print(i)
            i = i + 1
    except TypeError:
        logging.exception("Argument must be numeric")


check_all()
