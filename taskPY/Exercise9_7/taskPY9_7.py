""" Give me a word with three consecutive double letters. I’ll give you a couple of words
that almost qualify, but don’t. For example, the word committee, c-o-m-m-i-t-t-e-e. It
would be great except for the ‘i’ that sneaks in there. Or Mississippi: M-i-s-s-i-s-s-ip-p-i.
If you could take out those i’s it would work. But there is a word that has three
consecutive pairs of letters and to the best of my knowledge this may be the only word.
Of course there are probably 500 more but I can only think of one. What is the word?
Write a program to find it. """

import logging

logger = logging.getLogger()
logger.setLevel(logging.DEBUG)
fileHandler = logging.FileHandler('logs.log')
fileHandler.setFormatter(logging.Formatter(fmt='[%(asctime)s: %(levelname)s] %(message)s'))
logger.addHandler(fileHandler)


def three_pairs(word):
    """ function take a word and check that a word contains three pairs of letters """
    logging.info("Execute three_pairs() function")
    try:
        i = 0
        pairs = 0
        while i < len(word) - 1:
            if word[i] == word[i + 1]:
                pairs += 1
                if pairs == 3:
                    return True
                i = i + 2
            else:
                i = i + 1 - 2 * pairs
                pairs = 0
        return False
    except TypeError:
        logging.exception("Argument must be string")


def three_pairs_letters():
    """ function take word list and print words with three pairs letters """
    logging.info("Execute three_pairs_letters() function")
    try:
        file = open('words.txt')
        for line in file:
            word = line.strip()
            if three_pairs(word):
                print(word)
    except FileNotFoundError:
        logging.exception("File word.txt don't exist")


three_pairs_letters()
