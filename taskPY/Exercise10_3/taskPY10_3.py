""" Exercise 10.3. Write a function called middle that takes a list and returns a new list that contains
all but the first and last elements. """


import logging

logger = logging.getLogger()
logger.setLevel(logging.DEBUG)
fileHandler = logging.FileHandler('logs.log')
fileHandler.setFormatter(logging.Formatter(fmt='[%(asctime)s: %(levelname)s] %(message)s'))
logger.addHandler(fileHandler)


def middle(num_list):
    """ function takes a list and returns a new list that contains
    all but the first and last elements """
    logging.info("Execute middle() function")
    return x[1:-1]


x = [1, 2, 3, 4]
print(middle(x))
