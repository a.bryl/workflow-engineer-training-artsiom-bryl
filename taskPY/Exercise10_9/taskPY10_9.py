""" Exercise 10.9. Write a function that reads the file words.txt and builds a list with one element
per word. Write two versions of this function, one using the append method and the other using
the idiom t = t + [x]. """

import logging

logger = logging.getLogger()
logger.setLevel(logging.DEBUG)
fileHandler = logging.FileHandler('logs.log')
fileHandler.setFormatter(logging.Formatter(fmt='[%(asctime)s: %(levelname)s] %(message)s'))
logger.addHandler(fileHandler)


def list1():
    """ function read words from file and build a list using append """
    logging.info("Execute list1() function")
    try:
        first_list = []
        file = open('words.txt')
        for line in file:
            word = line.strip()
            first_list.append(word)
        return first_list
    except FileNotFoundError:
        logging.exception("File word.txt don't exist")


def list2():
    """function read words from file and build a list using the idiom t = t + [x] """
    logging.info("Execute list2() function")
    try:
        second_list = []
        file = open('words.txt')
        for line in file:
            word = line.strip()
            second_list += [word]
        return second_list
    except FileNotFoundError:
        logging.exception("File word.txt don't exist")


print(list1(), list2())
