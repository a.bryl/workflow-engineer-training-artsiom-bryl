""" Exercise 10.2. Write a function called cumsum that takes a list of numbers and returns the cumulative sum;
that is, a new list where the ith element is the sum of the first i + 1 elements from the original list. """


import logging

logger = logging.getLogger()
logger.setLevel(logging.DEBUG)
fileHandler = logging.FileHandler('logs.log')
fileHandler.setFormatter(logging.Formatter(fmt='[%(asctime)s: %(levelname)s] %(message)s'))
logger.addHandler(fileHandler)


def cumsum(num_list):
    """ function takes a list of numbers and returns the cumulative sum """
    try:
        logging.info("Execute cumsum() function")
        total = 0
        new_list = []
        for i in num_list:
            total += i
            new_list.append(total)
        return new_list
    except TypeError:
        logging.exception("Arguments must be numeric")


x = [1, 2, 3]
print(cumsum(x))
