## Exercise 8.4

The following functions are all intended to check whether a string contains any
lowercase letters, but at least some of them are wrong. For each function, describe what the function
actually does (assuming that the parameter is a string). 
```python
def any_lowercase1(s):
    for c in s:
        if c.islower():
            return True
        else:
            return False
```
The function ```any_lowercase1()``` checks if the first letter is lowercase a string. Returns ```True``` if the letter 
is lowercase and ```False``` if the letter is uppercase.
```python
def any_lowercase2(s):
    for c in s:
        if 'c'.islower():
            return 'True'
        else:
            return 'False'
```
Regardless of the input value, the function ```any_lowercase2()``` will always return ```True```, because the function 
checks if the string ```'c' ``` consists of lowercase letters or not.
```python
def any_lowercase3(s):
    for c in s:
        flag = c.islower()
    return flag
```
The function ```any_lowercase3()``` iterates over whether each letter in the string is lowercase or not, and writes 
the result: ```True``` or ```False``` to the variable ```flag```. Returns the result for the last letter only.
```python
def any_lowercase4(s):
    flag = False
    for c in s:
        flag = flag or c.islower()
    return flag
```
The function ```any_lowercase4()``` bruteforce checks if there is at least one lowercase letter in the string.
Initially, the variable ```flag``` is set to value ```False```. If the string contains at least one lowercase 
letter, then the value ```True``` will be written to the variable ```flag```.
```python
def any_lowercase5(s):
    for c in s:
        if not c.islower():
            return False
    return True
```
The function ```anylowercase()``` bruteforce checks if there is at least one non-lowercase letter in the string. 
If the string contains at least one non-lowercase letter, then the value ```False``` will be returned. If the string 
contains all lowercase letters, then the value ```True``` will be returned.
