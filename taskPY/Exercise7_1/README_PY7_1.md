# Information about doing exercise 7-1

## Questions and answers for requirements

The requirements for Exercise 7-1 are clear. There were no questions while completing the task.

## Information for launching

To launch the code, you need to export next modules:
- math
- logging

## Testing data

Testing data is in the attached screenshot.
