""" Copy the loop from Section 7.5 and encapsulate it in a function called mysqrt that
takes a as a parameter, chooses a reasonable value of x, and returns an estimate of the square root of
a.
To test it, write a function named test_square_root that prints a table like this:

a   mysqrt(a)     math.sqrt(a)  diff
-   ---------     ------------  ----
1.0 1.0           1.0           0.0
2.0 1.41421356237 1.41421356237 2.22044604925e-16
3.0 1.73205080757 1.73205080757 0.0
4.0 2.0           2.0           0.0
5.0 2.2360679775  2.2360679775  0.0
6.0 2.44948974278 2.44948974278 0.0
7.0 2.64575131106 2.64575131106 0.0
8.0 2.82842712475 2.82842712475 4.4408920985e-16
9.0 3.0           3.0           0.0

The first column is a number, a; the second column is the square root of a computed with mysqrt;
the third column is the square root computed by math.sqrt; the fourth column is the absolute value
of the difference between the two estimates."""

from math import sqrt
import logging

logger = logging.getLogger()
logger.setLevel(logging.DEBUG)
fileHandler = logging.FileHandler('logs7_1/logs.log')
fileHandler.setFormatter(logging.Formatter(fmt='[%(asctime)s: %(levelname)s] %(message)s'))
logger.addHandler(fileHandler)


def my_sqrt(a):
    """ function calculate square root """
    logging.info("Execute function my_sqrt.")
    x = a / 2
    while True:
        y = (x + a / x) / 2
        if y == x:
            break
        x = y
    return x


def table_leveling(s):
    """ function padding the string with spaces for correct display """
    return s + ' ' * (18 - len(s))


def test_square_root():
    """ function print table with square roots """
    logging.info("Execute function test_square_root")
    print('a' + 3 * ' ' + 'my_sqrt(a)' + 9 * ' ' + 'math.sqrt(a)' + 7 * ' ' + 'diff')
    print('-' + 3 * ' ' + 10 * '-' + 9 * ' ' + 12 * '-' + 7 * ' ' + 4 * '-')
    n = 1
    try:
        while n < 10:
            print(float(n), table_leveling(str(my_sqrt(n))), table_leveling(str(sqrt(n))),
                  table_leveling(str(abs(my_sqrt(n) - sqrt(n)))))
            n += 1
    except TypeError:
        logging.exception("Argument must be numeric")


test_square_root()
