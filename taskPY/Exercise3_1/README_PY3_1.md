# Information about doing exercise 3-1

## Questions and answers for requirements

The requirements for Exercise 3-1 are clear. There were no questions while completing the task.

## Information for launching

To launch the code, you don't need to install additional components except for Python 3.X.

## Testing data

Testing data is in the attached screenshot.
