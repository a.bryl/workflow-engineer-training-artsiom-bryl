# Exercise 1.1

It is a good idea to read this book in front of a computer so you can try out the
examples as you go.
Whenever you are experimenting with a new feature, you should try to make mistakes. For example,
in the “Hello, world!” program, what happens if you leave out one of the quotation marks? What if
you leave out both? What if you spell print wrong?
This kind of experiment helps you remember what you read; it also helps when you are programming,
because you get to know what the error messages mean. It is better to make mistakes now and on
purpose than later and accidentally.

1. In a print statement, what happens if you leave out one of the parentheses, or both?
2. If you are trying to print a string, what happens if you leave out one of the quotation marks,
or both?
3. You can use a minus sign to make a negative number like -2. What happens if you put a plus
sign before a number? What about 2++2?
4. In math notation, leading zeros are ok, as in 09. What happens if you try this in Python?
What about 011?
5. What happens if you have two values with no operator between them?

## Answers

1. The output will show a syntax error message. The coordinates (line number and character number) of the error 
   will also be indicated.
2. The output will show a syntax error message.
3. If you put "+" in front of a number, the interpreter will print this number without the "+" sign. If we enter 
   "2 ++ 2" into the interpreter, then the output will be "4", i.e. an ordinary addition operation will occur.
4. At the output, we will get a syntax error, because in Python it is not allowed to write numbers starting with 
   0 except for fractional numbers.
5. We will get a syntax error at the output.
