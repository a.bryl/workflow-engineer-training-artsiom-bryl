""" Exercise 10.5. Write a function called is_sorted that takes a list as a parameter and returns True
if the list is sorted in ascending order and False otherwise. """


import logging

logger = logging.getLogger()
logger.setLevel(logging.DEBUG)
fileHandler = logging.FileHandler('logs.log')
fileHandler.setFormatter(logging.Formatter(fmt='[%(asctime)s: %(levelname)s] %(message)s'))
logger.addHandler(fileHandler)


def is_sorted(any_list):
    """ function takes a list as a parameter and returns True
    if the list is sorted in ascending order and False otherwise """
    logging.info("Execute is_sorted() function")
    if any_list == sorted(any_list):
        return True
    else:
        return False


print(is_sorted([1, 2, 2]))
print(is_sorted(['b', 'a']))
