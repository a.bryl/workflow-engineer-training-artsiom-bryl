""" A string slice can take a third index that specifies the “step size”; that is, the number
of spaces between successive characters. A step size of 2 means every other character; 3 means every
third, etc.
A step size of -1 goes through the word backwards, so the slice [::-1] generates a reversed string.
Use this idiom to write a one-line version of is_palindrome from Exercise 6.3. """


import logging

logger = logging.getLogger()
logger.setLevel(logging.DEBUG)
fileHandler = logging.FileHandler('logs.log')
fileHandler.setFormatter(logging.Formatter(fmt='[%(asctime)s: %(levelname)s] %(message)s'))
logger.addHandler(fileHandler)


def is_palindrome(s):
    logging.info("Execute is_palindrome function")
    try:
        if s == s[::-1]:
            return True
        else:
            return False
    except TypeError:
        logging.exception("Argument must be string")


print(is_palindrome('Hello, World!'))
print(is_palindrome('level'))
print(is_palindrome('s'))
print(is_palindrome(716))
