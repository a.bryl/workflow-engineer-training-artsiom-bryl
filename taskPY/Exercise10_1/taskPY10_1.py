""" Exercise 10.1. Write a function called nested_sum that takes a list of lists of integers and adds up
the elements from all of the nested lists. """


import logging

logger = logging.getLogger()
logger.setLevel(logging.DEBUG)
fileHandler = logging.FileHandler('logs.log')
fileHandler.setFormatter(logging.Formatter(fmt='[%(asctime)s: %(levelname)s] %(message)s'))
logger.addHandler(fileHandler)


def nested_sum(list_of_lists):
    """ function takes a list of lists of integers and adds up
    the elements from all of the nested lists """
    logging.info("Execute nested_sum() function")
    try:
        sum_list = 0
        for nested_list in list_of_lists:
            sum_list += sum(nested_list)
        return sum_list
    except TypeError:
        logging.exception("Arguments must be numeric")


t = [[1, 2], [3], [4, 5, 6]]
print(nested_sum(t))
