# Information about doing exercise 8-2

## Questions and answers for requirements

The requirements for Exercise 8-2 are clear. There were no questions while completing the task.

## Information for launching

To launch the code, you don't need to export any modules or libraries.

## Testing data

Testing data is in the attached screenshot.
