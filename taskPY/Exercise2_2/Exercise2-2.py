from math import pi
from datetime import timedelta

""" The volume of a sphere with radius r is 4/3*π*r^3. What is the volume of a sphere with radius 5? """

radius = 5
volume = round(4 / 3 * pi * radius ** 3, 3)
print('The volume of a sphere with radius 5 is ' + str(volume))

""" Suppose the cover price of a book is $24.95, but bookstores get a 40% discount. Shipping costs
$3 for the first copy and 75 cents for each additional copy. What is the total wholesale cost for
60 copies? """

price = 24.95
discount = 40
first_shipping_cost = 3
additional_shipping_cost = 0.75
copies = 60
total_price = (price - price * 40 / 100) * copies
total_shipping_coast = first_shipping_cost + additional_shipping_cost * (copies - 1)
wholesale_cost = round(total_price + total_shipping_coast, 2)
print('$' + str(wholesale_cost) + ' is the total wholesale cost for 60 copies')

"""  If I leave my house at 6:52 am and run 1 mile at an easy pace (8:15 per mile), then 3 miles at
tempo (7:12 per mile) and 1 mile at easy pace again, what time do I get home for breakfast? """

leave_time = timedelta(hours=6, minutes=52, seconds=0)
easy_pace = timedelta(hours=0, minutes=8, seconds=15)
tempo_pace = timedelta(hours=0, minutes=7, seconds=12)
easy_distance_time = 2 * easy_pace
tempo_distance_time = 3 * tempo_pace
comeback_time = leave_time + easy_distance_time + tempo_distance_time
print(comeback_time)
