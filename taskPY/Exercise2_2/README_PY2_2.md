# Information about doing exercise 2-2

## Questions and answers for requirements

The requirements for Exercise 2-2 are clear. There were no questions while completing the task.

## Information for launching

To launch the code, you need to import next additional libraries:
- math
- datetime

## Testing data

Testing data is in the attached screenshot.
