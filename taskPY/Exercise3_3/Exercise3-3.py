""" Note: This exercise should be done using only the statements and other features we
have learned so far.
1. Write a function that draws a grid like the following.
The output of these statements is '+ -' on the same line. The output from the next print
statement would begin on the next line.
2. Write a function that draws a similar grid with four rows and four columns. """


def grid(column, line):
    x = ('+ - - - - ' * column + '+')
    y = ('\n' + '|         ' * (column + 1))
    print(((x + 4 * y) + '\n') * line + x)


grid(2, 2)

grid(4, 4)
