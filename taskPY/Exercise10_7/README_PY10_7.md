# Information about doing exercise 10-7

## Questions and answers for requirements

The requirements for Exercise 10-7 are clear. There were no questions while completing the task.

## Information for launching

To launch the code, you need to export next modules:
- logging

## Testing data

Testing data is in the attached screenshot.
