""" Exercise 10.7. Write a function called has_duplicates that takes a list and returns True if there
is any element that appears more than once. It should not modify the original list. """


import logging

logger = logging.getLogger()
logger.setLevel(logging.DEBUG)
fileHandler = logging.FileHandler('logs.log')
fileHandler.setFormatter(logging.Formatter(fmt='[%(asctime)s: %(levelname)s] %(message)s'))
logger.addHandler(fileHandler)


def has_duplicates(any_list):
    """ function takes a list and returns True if there
    is any element that appears more than once """
    logging.info("Execute has_duplicates() function")
    try:
        if len(any_list) == len(set(any_list)):
            return False
        else:
            return True
    except TypeError:
        logging.exception("Arguments must be list")


print(has_duplicates('tommarvoloriddle'))
print(has_duplicates([1, 2, 3, 4, 5, 6, 7, 8, 9, 0]))
print(has_duplicates(17))
