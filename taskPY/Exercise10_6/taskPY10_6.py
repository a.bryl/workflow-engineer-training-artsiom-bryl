""" Exercise 10.6. Two words are anagrams if you can rearrange the letters from one to spell the other.
Write a function called is_anagram that takes two strings and returns True if they are anagrams. """


import logging

logger = logging.getLogger()
logger.setLevel(logging.DEBUG)
fileHandler = logging.FileHandler('logs.log')
fileHandler.setFormatter(logging.Formatter(fmt='[%(asctime)s: %(levelname)s] %(message)s'))
logger.addHandler(fileHandler)


def is_anagram(first_list, second_list):
    """ function takes two strings and returns True if they are anagrams """
    try:
        logging.info("Execute is_anagram() function")
        if sorted(first_list) == sorted(second_list):
            return True
        else:
            return False
    except TypeError:
        logging.exception("Arguments must be string")


print(is_anagram('tommarvoloriddle', 'iamlordvoldemort'))
print(is_anagram('bilbo', 'baggins'))
print(is_anagram(17, 15))
