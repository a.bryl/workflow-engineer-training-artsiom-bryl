""" A function object is a value you can assign to a variable or pass as an argument. For
example, do_twice is a function that takes a function object as an argument and calls it twice:

def do_twice(f):
    f()
    f()

Here’s an example that uses do_twice to call a function named print_spam twice.

def print_spam():
    print('spam')
do_twice(print_spam)

1. Type this example into a script and test it. """


def do_twice(f):
    f()
    f()


def print_spam():
    print('spam')


do_twice(print_spam)

""" 2. Modify do_twice so that it takes two arguments, a function object and a value, and calls the
function twice, passing the value as an argument. """


def do_twice(f, n):
    f(n)
    f(n)


def print_spam(s):
    print(s)


do_twice(print_spam, 'spam')

""" 3. Copy the definition of print_twice from earlier in this chapter to your script. """

""" Here is a definition for a function that takes an argument:

def print_twice(bruce):
    print(bruce)
    print(bruce)

This function assigns the argument to a parameter named bruce. When the function is
called, it prints the value of the parameter (whatever it is) twice. """

""" 4. Use the modified version of do_twice to call print_twice twice, passing 'spam' as an
argument. """


def do_twice(f, s):
    f(s)
    f(s)


def print_twice(s):
    print(s)
    print(s)


do_twice(print_twice, 'spam')

""" 5. Define a new function called do_four that takes a function object and a value and calls the
function four times, passing the value as a parameter. There should be only two statements in
the body of this function, not four. """


def do_twice(f, s):
    f(s)
    f(s)


def do_four(f, s):
    do_twice(f, s)
    do_twice(f, s)


do_four(print, 'do_four')
