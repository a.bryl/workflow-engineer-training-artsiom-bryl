""" The time module provides a function, also named time, that returns the current
Greenwich Mean Time in “the epoch”, which is an arbitrary time used as a reference point. On
UNIX systems, the epoch is 1 January 1970.
# >>> import time
# >>> time.time()
1437746094.5735958
Write a script that reads the current time and converts it to a time of day in hours, minutes, and
seconds, plus the number of days since the epoch. """

from time import *

current_time = time()

seconds_per_minute = 60
seconds_per_hour = 3600
seconds_per_day = 86400

seconds = int(current_time)

days = seconds // seconds_per_day
seconds = seconds % seconds_per_day

hours = seconds // seconds_per_hour
seconds = seconds % seconds_per_hour

minutes = seconds // seconds_per_minute
seconds = seconds % seconds_per_minute

print("{0:.0f} days, {1:.0f} hours, {2:.0f} minutes, {3:.0f} seconds.".format(days, hours, minutes, seconds))
