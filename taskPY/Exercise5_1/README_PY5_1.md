# Information about doing exercise 5-1

## Questions and answers for requirements

The requirements for Exercise 5-1 are clear. There were no questions while completing the task.

## Information for launching

To launch the code, you need to import module time.

## Testing data

Testing data is in the attached screenshots.
