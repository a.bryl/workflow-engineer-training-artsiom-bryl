## Feature engineering guidelines checklist

# Read the requirements 

| Item | Is confirmed? (Y/N) |
|---|---|
| Make sure that you understand every sentence and every word in both functional and non-functional requirements defined for the task | |
| Communicate and address every questions and potential misunderstanding you have before starting the coding | |
| Suggest workarounds and try to make your own decision if some non-critical information is not explicitly defined in the requirements | |
| Capture all your questions, workarounds and decision in the task tracker | |
| Be able to explain how you are going to confirm that the features address all requirements, both functional, non-functional and architectural | |
| Suggest simpler implementation or changes in requirements to simplify the implementation if it does not contradict task's goals | |

# Implement the feature 

| Item | Is confirmed? (Y/N) |
|---|---|
| The code shall cover both positive and negative scenarios | |
| The code shall work properly with edge cases of the potential input | |
| The code shall properly handle errors in input data and 3rd party system call responses | |
| In case of errors the system shall either continue to work and write error information into logs or shutdown and provide a clear response to user with the explanation of the source of error | |
| The code shall log the necessary information for potential troubleshooting | |
| Reproduce the issue and understand the root cause before fixing it | |
| Look for ways to automate your work and avoid creating repetitive boilerplate code | |
| Push the code at least daily | |
| Create good UI when it is not defined explicitly using best practices and UI frameworks | |
| Update the technical documentation of the system | |

# Communicate 

| Item | Is confirmed? (Y/N) |
|---|---|
| Add helpful information for the QA team on how to better test the implementation to the ticket tracker (potential impact in the complex system, which areas may be affected, how to access the functionality that does not have UI, etc | |
| Provide updates for the feature implementation in a ticket tracker if it takes more than a day | |
| Notify lead engineer or project manager if the implementation takes longer that you planned | |
| Ask for help if you stuck and do not know how to proceed with the implementation | |
| Communicate any configuration or other dependencies necessary for production release of the feature (infrastructure configuration updates, adding new parameters to the build, running one-time migrations or other scripts, etc.) and capture that information in the ticket | |

# Test the feature 

| Item | Is confirmed? (Y/N) |
|---|---|
| Make sure the code builds without any errors and warnings (including coding guidelines style checks) | |
| Do not push the code that you did not run | |
| Test all positive and negative scenarios | |
| Test edge cases and error handling | |
| Test the feature in a non-local environment after the push | |